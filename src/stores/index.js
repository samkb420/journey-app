// store to get journey details

import ApiClient from "@/../Axios";

import { createStore } from "vuex";



export default createStore({
	state: {
		journey: [],
		loading: true,
		error: false,
		errorMessage: '',
	},
	mutations: {
		setjourney(state, journey) {
			state.journey = journey;

		},
		setLoading(state, loading) {
			state.loading = loading;
		}

	},
	actions: {
		async getjourney({ commit, state }, page ) {

			try {
				
				if (!page){
					page = 1;

				}
				const response = await ApiClient.get(`/journeys?page=${page}`);
				console.log(page)
				commit('setjourney', response.data);
                // set loading to falsepayloadpage
                commit('setLoading', false);

				console.log(response.data.results);

				
			} catch (error) {
				console.log(error);
			}
		}
	},
	modules: {
	},
	getters: {
		journey: state => state.journey,
	}
});



